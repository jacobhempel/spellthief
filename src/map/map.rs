extern crate nom;
extern crate find_folder;

use nom::*;
use nom::types::*;

use map::tile::*;
use engine::resolution::*;

use std::str::FromStr;
use std::error::Error;
use std::fs::File;
use std::io::{BufRead, BufReader};



pub struct Map {
    pub width: u32,
    pub height: u32,
    pub tile_size: u32,
    pub zone_name: String,
    pub tile_map: Vec<Tile>,
}

// got some map modification fucntions that might be useful down the line...
#[allow(dead_code)]
impl Map {
    pub fn new(width: u32, height: u32) -> Map {
        let mut new_map = Map {
            width: width,
            height: height,
            tile_size: get_screen_width() / 40,
            zone_name: "".to_string(),
            tile_map: Vec::with_capacity((width * height) as usize ),
        };
        let floor = Tile::new('x', TileType::Floor);
        new_map.fill_all_with(floor);

        new_map
    }

    pub fn is_passable(&self, x: f64, y: f64) -> bool {
        // checks to make sure you're not off the left/top edge
        if x < 0.1 || y < 0.1 {
            false
        }
        // next two check for right/bottom edges
        else if (x as u32 / self.tile_size) >= self.width - 1 {
            false
        }
        else if (y as u32 / self.tile_size) >= self.height - 1 {
            false
        }
        // checks all 4 corners of player model for collision
        else if self.get_tile_pos((x + 6.0) as u32, (y + 4.0) as u32).passable &&
                self.get_tile_pos((x + 42.0) as u32, (y + 4.0) as u32).passable &&
                self.get_tile_pos((x + 6.0) as u32, (y + 42.0) as u32).passable &&
                self.get_tile_pos((x + 42.0) as u32, (y + 42.0) as u32).passable
        {
            true
        }
        else {
            false
        }
    }

    // returns a reference to a tile in the map
    pub fn get_tile(&self, x: u32, y: u32) -> &Tile {
        let index = self.index(x, y);
        &self.tile_map[index]
    }

    pub fn get_tile_pos(&self, x: u32, y: u32) -> &Tile {
        let tile_x = x / self.tile_size;
        let tile_y = y / self.tile_size;
        let index = self.index(tile_x, tile_y);
        &self.tile_map[index]
    }

    pub fn set_name(&mut self, new_name: String) {
        self.zone_name = new_name;
    }

    pub fn get_name(&self) -> &str {
        self.zone_name.as_str()
    }

    pub fn fill_all_with(&mut self, fill_tile: Tile) {
        for y in 0..self.height {
            for x in 0..self.width {
                self.insert_tile(fill_tile, x, y);
            }
        }
    }
    pub fn replace_all_with(&mut self, fill_tile: Tile) {
        for y in 0..self.height {
            for x in 0..self.width {
                self.replace_tile(fill_tile, x, y);
            }
        }
    }

    pub fn insert_tile(&mut self, new_tile: Tile, x: u32, y: u32) {
        let index = self.index(x,y);
        self.tile_map.insert(index, new_tile);
    }
    pub fn replace_tile(&mut self, new_tile: Tile, x: u32, y: u32) {
        let index = self.index(x,y);
        self.tile_map[index] = new_tile;
    }

    pub fn build_from_file(file_name: &str) -> Map {
        let configs = find_folder::Search::ParentsThenKids(3,3).for_folder("configs").unwrap();
        let config_path = configs.join(file_name);

        let file = match File::open(&config_path) {
            Err(why) => panic!("couldn't open file because {}", why.description()),
            Ok(file) => file,
        };

        let mut lines = BufReader::new(file).lines();

        let size = lines.next().unwrap().unwrap();

        let size_tuple = parse_size(CompleteStr(&size)).unwrap().1;

        let mut new_map = Map::new(size_tuple.0, size_tuple.1);

        let wall = Tile::new('x', TileType::Wall);


        for line in lines {
            let line = line.unwrap();
            let wall_tuple = parse_tile(CompleteStr(&line)).unwrap().1;
            new_map.replace_tile(wall, wall_tuple.1, wall_tuple.2);
        }
        new_map
    }

    fn index(&self, x: u32, y: u32) -> usize {
        // println!("{}", (y * self.width + x) as usize);
        (y * self.width + x) as usize
    }
}

named!(parse_size(CompleteStr) -> (u32, u32),
    pair!(
        ws!(map_res!(digit, |CompleteStr(s)| u32::from_str(s))),
        ws!(map_res!(digit, |CompleteStr(s)| u32::from_str(s)))
    )
);

named!(parse_tile(CompleteStr) -> (CompleteStr, u32, u32),
    tuple!(
        ws!(tag!("Wall")),
        ws!(map_res!(digit, |CompleteStr(s)| u32::from_str(s))),
        ws!(map_res!(digit, |CompleteStr(s)| u32::from_str(s)))
    )
);
