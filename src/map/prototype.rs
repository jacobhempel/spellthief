extern crate find_folder;
extern crate nom;

use nom::*;
use nom::types::*;

use engine::texture::*;

use std::str::FromStr;
use std::fs::File;
use std::error::Error;
use std::io::{BufRead, BufReader};

pub struct ProtoTexture {
    pub size_x: f64,
    pub size_y: f64,
    pub pos_x: f64,
    pub pos_y: f64,
    pub layer: RenderLayer,
    pub png: String,
}

impl ProtoTexture {
    pub fn new(xs: f64, ys: f64, xp: f64, yp: f64, layer: u8, file_name: &str) -> ProtoTexture {
        ProtoTexture {
            size_x: xs,
            size_y: ys,
            pos_x: xp,
            pos_y: yp,
            layer: RenderLayer::get_layer(layer),
            png: file_name.to_string(),
        }
    }
}

pub struct ProtoTextureMap {
    pub map: Vec<ProtoTexture>,
}

impl ProtoTextureMap {
    pub fn new() -> ProtoTextureMap {
        ProtoTextureMap {
            map: Vec::new(),
        }
    }

    pub fn build_from_file(file_name: &str) -> ProtoTextureMap {
        let configs = find_folder::Search::ParentsThenKids(3,3).for_folder("configs").unwrap();
        let config_path = configs.join(file_name);

        let file = match File::open(&config_path) {
            Err(why) => panic!("couldn't open file because {}", why.description()),
            Ok(file) => file,
        };

        let lines = BufReader::new(file).lines();

        let mut tex_map = ProtoTextureMap::new();

        for line in lines {
            let line = line.unwrap();
            let tex_tuple = parse_tex(CompleteStr(&line)).unwrap().1;
            tex_map.add_tex(tex_tuple.0 , tex_tuple.1 ,tex_tuple.2 ,tex_tuple.3 ,tex_tuple.4, &tex_tuple.5);
        }

        tex_map



    }

    fn add_tex(&mut self, xs: f64, ys: f64, xp: f64, yp: f64, layer: u8, file_name: &str) {
        let tex = ProtoTexture::new(xs, ys, xp, yp, layer, file_name);
        self.map.push(tex);
    }
}

named!(parse_tex(CompleteStr) -> (f64, f64, f64, f64, u8, CompleteStr),
    tuple!(
        ws!(double),
        ws!(double),
        ws!(double),
        ws!(double),
        ws!(map_res!(digit, |CompleteStr(s)| u8::from_str(s))),
        ws!(rest)
    )
);
