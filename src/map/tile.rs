use nom::types::CompleteStr;

#[derive(Debug, Copy, Clone)]
pub struct Tile {
    pub texture: char,
    pub tile_type: TileType,
    pub passable: bool,
    pub blocked: bool,
    pub occupied: bool,
}

impl Tile {
    pub fn new(new_tex: char, tile: TileType) -> Tile {
        let mut new_tile = Tile {
            texture: new_tex,
            tile_type: tile,
            passable: true,
            blocked: false,
            occupied: false,
        };
        if tile == TileType::Wall {
            new_tile.passable = false;
            new_tile.blocked = true;
            new_tile.occupied = false;
        }
        else {}
        new_tile
    }
    #[allow(dead_code)]
    pub fn tile_from_completestr(input: CompleteStr) -> TileType {
        if input == CompleteStr("Wall") {
            TileType::Wall
        }
        else {
            TileType::Floor
        }
    }
}

#[derive(Eq, PartialEq, Ord, PartialOrd, Debug, Copy, Clone)]
pub enum TileType {
    Floor,
    Wall,
}

impl TileType {
    #[allow(dead_code)]
    pub fn to_str(self) -> String {
        match self {
            TileType::Floor => "Floor".to_string(),
            TileType::Wall  => "Wall".to_string(),
        }
    } 
}
