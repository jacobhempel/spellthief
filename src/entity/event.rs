use crossbeam::channel::*;

use engine::playerposition::Direction;

#[derive(Clone)]
pub struct EventQueue {
    tx:      Sender<EntityEvent>,
    rx:      Receiver<EntityEvent>,
}

impl EventQueue {
    pub fn new() -> EventQueue {
        let (ntx, nrx) = unbounded();
        EventQueue {
            tx: ntx,
            rx: nrx,
        }
    }

    pub fn push(&self, event: EntityEvent) {
        let send = self.tx.send(event);
        match send {
            Ok(_) => {},
            Err(e) => panic!("panicked when trying to add event to EventQueue \nError was {:?}", e),
        }
    }

    pub fn pop(&self) -> Result<EntityEvent, TryRecvError> {
        self.rx.try_recv()
    }

    pub fn is_empty(&self) -> bool {
        self.rx.is_empty()
    }
}

#[derive(Clone, Debug, PartialEq, PartialOrd)]
pub enum EntityEvent {
    Interact(Direction),
}