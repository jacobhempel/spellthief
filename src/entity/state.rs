use engine::animation::*;

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub enum State {
    Idle,
    Walk,
    Run,
    Attack,
    Cast,
    Interact,
    Closed,
    Open,
}

#[allow(dead_code)]
impl State {
    pub fn as_int(&self) -> i32 {
        match self {
            State::Idle     => 0,
            State::Walk     => 1,
            State::Run      => 2,
            State::Attack   => 3,
            State::Cast     => 4,
            State::Interact => 5,
            State::Closed   => 6,
            State::Open     => 7,
        }
    }
    pub fn as_anitype(&self) -> AnimationType {
        match self {
            State::Idle     => AnimationType::Idle,
            State::Walk     => AnimationType::Walk,
            State::Run      => AnimationType::Run,
            State::Attack   => AnimationType::Attack,
            State::Cast     => AnimationType::Cast,
            State::Interact => AnimationType::Interact,
            State::Closed   => AnimationType::Idle,
            State::Open     => AnimationType::Interact,
        }
    }
}

#[allow(dead_code)]
pub fn anitype_from_str(input: &str) -> State {
    match input {
        "Idle"     => State::Idle,
        "Walk"     => State::Walk,
        "Attack"   => State::Attack,
        "Run"      => State::Run,
        "Cast"     => State::Cast,
        "Interact" => State::Interact,
        _          => State::Idle,
    }
}
