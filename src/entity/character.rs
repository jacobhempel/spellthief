use piston_window::*;
use sdl2_window::Sdl2Window;

use engine::playerposition::*;
use engine::input::*;
use engine::camera::*;
use engine::texture::*;
use engine::animation_set::*;

use map::map::*;

use entity::entity::*;
use entity::state::*;
use entity::event::*;

#[derive(Clone)]
pub struct PlayerCharacter {
    character_name: String,

    pub pos: PlayerPosition,

    pub eventq: EventQueue,

    state: State,
    state_timer: u64,

    animation_set: AnimationSet,
}

impl PlayerCharacter {
    pub fn new(window: &mut PistonWindow<Sdl2Window>, start: PlayerPosition,
                animation_file: &str) -> PlayerCharacter {
        let pc = PlayerCharacter {
            character_name: "PLAYER".to_string(),
            pos: start,
            eventq: EventQueue::new(),
            state: State::Idle,
            state_timer: 0,
            animation_set: AnimationSet::new(animation_file, window),
        };
        println!("initialized PC named: {}", pc.character_name);
        pc
    }

    pub fn update_frame(&mut self, ticks: u64) {
        let mut texture = self.animation_set.update_frame(self.pos.facing.get().cardinal(), self.state.as_anitype(), ticks);
        texture.change_pos(self.pos.x - self.animation_set.offset_x as f64, self.pos.y - self.animation_set.offset_x as f64);
    }
}

// trait implementations for the player character
impl Entity for PlayerCharacter {
    // true if this thing wants to move during update()
    fn is_mv(&self) -> bool {
        true
    }

    // resolves inputs and moves entity where it needs to go
    fn mv(&mut self, input: &InputEngine, map: &Map, camera: &mut Camera, ticks: u64) {
        self.resolve_facing(input, camera);
        self.resolve_acceleration(input, ticks);
        self.pos.update(map, ticks);   
    }
    fn stop_mv(&mut self) {
        self.pos.x_vel = 0.0;
        self.pos.y_vel = 0.0;
    }


    // returns true of the entity can be moved through
    fn is_passable(&self) -> bool {
        false
    }

    // returns the texture that should be displayed by the entity given its current state
    fn get_frame(&mut self, _ticks: u64) -> RenderTexture {
        let mut frame = self.animation_set.get_active_frame();
        frame.change_pos(self.pos.x - self.animation_set.offset_x as f64, self.pos.y - self.animation_set.offset_x as f64);
        frame
    }

    fn get_pos(&self) -> (f64, f64) {
        (self.pos.x, self.pos.y)
    }

    fn send_event(&self, event: EntityEvent) {
        self.eventq.push(event);
    }

    fn interact(&self, entities: &Vec<Box<Entity>>) {
        let xmin; 
        let xmax;
        let ymin;
        let ymax;

        match self.pos.facing.get().cardinal() {
            Direction::North => {
                xmin = self.pos.x - 32.0;
                xmax = self.pos.x + 80.0;
                ymin = self.pos.y - 100.0;
                ymax = self.pos.y;
            }
            Direction::East  => {
                xmin = self.pos.x + 44.0;
                xmax = self.pos.x + 144.0;
                ymin = self.pos.y - 32.0;
                ymax = self.pos.y + 80.0;
            }
            Direction::South => {
                xmin = self.pos.x - 32.0;
                xmax = self.pos.x + 80.0;
                ymin = self.pos.y + 45.0;
                ymax = self.pos.y + 144.0;
            }
            Direction::West  => {
                xmin = self.pos.x - 100.0;
                xmax = self.pos.x;
                ymin = self.pos.y - 32.0;
                ymax = self.pos.y + 80.0;
            }
            _ => {
                {
                xmin = self.pos.x;
                xmax = self.pos.x;
                ymin = self.pos.y;
                ymax = self.pos.y;
            }
            }
        }

        for entity in entities.iter() {
            let (x,y) = entity.get_pos();
            if x > xmin && x < xmax && y > ymin && y < ymax {
                entity.send_event(EntityEvent::Interact(self.pos.facing.get().cardinal()));
            }
        }


    }


    fn update(&mut self, map: &Map, input: &InputEngine, camera: &mut Camera, entities: &Vec<Box<Entity>>, ticks: u64) {
        self.state_timer += ticks;
        let mut events = Vec::new();

        while !self.eventq.is_empty() {
            events.push(self.eventq.pop().unwrap());
        }

        for event in events {
            println!("PC got event: {:?}", event);
        }

        match self.state {
            State::Walk => {
                // transition to idle state from walking
                self.resolve_facing(input, camera);
                self.resolve_acceleration(input, ticks);
                self.pos.update(map, ticks);

                if input.keys.contains(&Key::E) {
                    self.stop_mv();
                    self.state_timer = 0;
                    self.state = State::Interact;
                } else if self.pos.x_vel == 0.0 && self.pos.y_vel == 0.0 {
                    self.state = State::Idle;
                }
            }

            State::Idle => {
                self.resolve_facing(input, camera);
                self.resolve_acceleration(input, ticks);
                self.pos.update(map, ticks);

                if input.keys.contains(&Key::E) {
                    self.state_timer = 0;
                    self.state = State::Interact;
                }

                if self.pos.x_vel != 0.0 || self.pos.y_vel != 0.0 {
                    self.state = State::Walk;
                }
            }

            State::Interact => {
                if self.state_timer == 152 {
                    self.interact(entities);
                } 
                if self.state_timer > 340 {
                    self.state = State::Idle;
                }
            }
            _ => {
                panic!("somehow we got to {:?}", self.state);
            }
        }
    }

    fn box_clone(&self) -> Box<Entity> {
        Box::new((*self).clone())
    }
}

// ugly shit down here 
impl PlayerCharacter {
    fn resolve_acceleration(&mut self, input: &InputEngine, ticks: u64) {
        let accel = self.pos.accel;
        if input.keys.contains(&Key::W) {
            self.pos.accelerate(0.0, -accel, ticks);
        }
        if input.keys.contains(&Key::A) {
            self.pos.accelerate(-accel, 0.0, ticks);
        }
        if input.keys.contains(&Key::S) {
            self.pos.accelerate(0.0, accel, ticks);
        }
        if input.keys.contains(&Key::D) {
            self.pos.accelerate(accel, 0.0, ticks);
        }
        // deceleration occurs here
        let decel = self.pos.accel / 2.0;
        if self.pos.x_vel > 0.0 {
            self.pos.x_vel -= decel * (ticks as f64);
            if self.pos.x_vel < 0.0 {
                self.pos.x_vel = 0.0;
            }
        }
        if self.pos.x_vel < 0.0 {
            self.pos.x_vel += decel * (ticks as f64);
            if self.pos.x_vel > 0.0 {
                self.pos.x_vel = 0.0;
            }
        }
        if self.pos.y_vel > 0.0 {
            self.pos.y_vel -= decel * (ticks as f64);
            if self.pos.y_vel < 0.0 {
                self.pos.y_vel = 0.0;
            }
        }
        if self.pos.y_vel < 0.0 {
            self.pos.y_vel += decel * (ticks as f64);
            if self.pos.y_vel > 0.0 {
                self.pos.y_vel = 0.0;
            }
        }
    }

    fn resolve_facing(&mut self, input: &InputEngine, camera: &mut Camera) {
        // bunch of code here, if we want "FACING" to be determined by input
        if input.keys.contains(&Key::W) && !input.keys.contains(&Key::D) &&
          !input.keys.contains(&Key::S) && !input.keys.contains(&Key::A) {
            self.pos.facing.set(Direction::North);
            camera.facing(Direction::North);
        }
        else if input.keys.contains(&Key::W) && input.keys.contains(&Key::D) &&
                !input.keys.contains(&Key::S) && !input.keys.contains(&Key::A) {
            self.pos.facing.set(Direction::Northeast);
            camera.facing(Direction::Northeast);
        }
        else if !input.keys.contains(&Key::W) && input.keys.contains(&Key::D) &&
                 !input.keys.contains(&Key::S) && !input.keys.contains(&Key::A) {
            self.pos.facing.set(Direction::East);
            camera.facing(Direction::East);
        }
        else if !input.keys.contains(&Key::W) && input.keys.contains(&Key::D) &&
                 input.keys.contains(&Key::S) && !input.keys.contains(&Key::A) {
            self.pos.facing.set(Direction::Southeast);
            camera.facing(Direction::Southeast);
        }
        else if !input.keys.contains(&Key::W) && !input.keys.contains(&Key::D) &&
                 input.keys.contains(&Key::S) && !input.keys.contains(&Key::A) {
            self.pos.facing.set(Direction::South);
            camera.facing(Direction::South);
        }
        else if !input.keys.contains(&Key::W) && !input.keys.contains(&Key::D) &&
                input.keys.contains(&Key::S) && input.keys.contains(&Key::A) {
            self.pos.facing.set(Direction::Southwest);
            camera.facing(Direction::Southwest);
        }
        else if !input.keys.contains(&Key::W) && !input.keys.contains(&Key::D) &&
                !input.keys.contains(&Key::S) && input.keys.contains(&Key::A) {
            self.pos.facing.set(Direction::West);
            camera.facing(Direction::West);
        }
        else if input.keys.contains(&Key::W) && !input.keys.contains(&Key::D) &&
                !input.keys.contains(&Key::S) && input.keys.contains(&Key::A) {
            self.pos.facing.set(Direction::Northwest);
            camera.facing(Direction::Northwest);
        }
    }
}
