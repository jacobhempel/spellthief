use engine::input::*;
use map::map::*;

use entity::state::*;
use entity::entity::*;

trait StateMachine {
    fn get_next_state(&self, input: &InputEngine, map: &Map, entities: &Vec<Box<Entity>>) -> State; 
}