use piston_window::*;
use sdl2_window::Sdl2Window;

use engine::playerposition::*;
use engine::animation_set::*;
use engine::camera::*; 
use engine::input::*;
use engine::texture::*;

use map::map::*;

use entity::entity::Entity;
use entity::state::State;
use entity::event::*;

#[derive(Clone)]
pub struct TreasureChest {
    pub pos:           PlayerPosition,
    pub state:         State,
    pub animation_set: AnimationSet,
    pub eventq:        EventQueue,
    pub state_timer:   u64,
}

impl TreasureChest {
    pub fn new(window: &mut PistonWindow<Sdl2Window>, x: f64, y: f64, animation_file: &str) -> TreasureChest {
        TreasureChest {
            pos:           PlayerPosition::new(x, y, 0),
            state:         State::Closed,
            animation_set: AnimationSet::new(animation_file, window),
            eventq:        EventQueue::new(),
            state_timer:   0,
        }        
    }
}

impl Entity for TreasureChest {
    fn get_pos(&self) -> (f64, f64) {
        (self.pos.x, self.pos.y)
    }

    fn is_mv(&self) -> bool {
        false
    }
    fn mv(&mut self, _input: &InputEngine, _map: &Map, _camera: &mut Camera, _ticks: u64) {}
    fn stop_mv(&mut self) {}

    fn send_event(&self, event: EntityEvent) {
        self.eventq.push(event);
    }

    fn is_passable(&self) -> bool {
        false
    }

    fn get_frame(&mut self, ticks: u64) -> RenderTexture {
        let mut texture = self.animation_set.update_frame(self.pos.facing.get().cardinal(), self.state.as_anitype(), ticks);
        texture.change_pos(self.pos.x - self.animation_set.offset_x as f64, self.pos.y - self.animation_set.offset_x as f64);
        texture
    }

    fn update(&mut self, _map: &Map, _input: &InputEngine, _camera: &mut Camera, _entities: &Vec<Box<Entity>>, ticks: u64) {
        self.state_timer += ticks;
        let mut events = Vec::new();

        while !self.eventq.is_empty() {
            events.push(self.eventq.pop().unwrap());
        }  

        match self.state {
            State::Closed => {
                if events.contains(&EntityEvent::Interact(Direction::North)) {
                    self.state_timer = 0;
                    self.state = State::Open;
                    println!("opened the chest!!");
                }
            }
            State::Open => {
                if self.state_timer > 6000 {
                    self.state = State::Closed;
                    println!("chest closed by itself");
                }
            }
            _ => {}
        }
    }

    fn box_clone(&self) -> Box<Entity> {
        Box::new((*self).clone())
    }
}