use engine::texture::RenderTexture;
use engine::input::*;
use engine::camera::*;

use map::map::*;

use entity::event::*;

// declares functions that must be implemented by types that _are_ Entity
pub trait Entity {
    fn get_pos(&self) -> (f64, f64);

    fn is_mv(&self) -> bool;
    fn mv(&mut self, input: &InputEngine, map: &Map, camera: &mut Camera, ticks: u64);
    fn stop_mv(&mut self);

    fn interact(&self, _entities: &Vec<Box<Entity>>) {}

    fn send_event(&self, event: EntityEvent);

    fn is_passable(&self) -> bool;

    fn get_frame(&mut self, ticks: u64) -> RenderTexture;

    fn update(&mut self, map: &Map, input: &InputEngine, camera: &mut Camera, entities: &Vec<Box<Entity>>, ticks: u64);

    // all entities have to implement this so that we can copy boxed trait object entities all over the place
    // this implementation should work for all entities that are themselves Clone 
    // just copy pasta it into your new entity
    // 
    // fn box_clone(&self) -> Box<Entity> {
    //     Box::new((*self).clone())
    // }
    fn box_clone(&self) -> Box<Entity>;
}

impl Clone for Box<Entity> {
    fn clone(&self) -> Box<Entity> {
        self.box_clone()
    }
}