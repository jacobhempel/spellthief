extern crate piston;
extern crate piston_window;


use piston_window::*;
use std::collections::HashSet;

pub struct InputEngine {
    pub keys: HashSet<piston_window::Key>
}

// simple hash map of input - prevents us from having to debounce our own buttons (gross)
impl InputEngine {
    pub fn new() -> InputEngine {
        let inpeng = InputEngine {
            keys: HashSet::new(),
        };
        inpeng
    }
    pub fn get(&mut self, e: &Event) {
        if let Some(Button::Keyboard(key)) = e.press_args() {
            self.keys.insert(key);
            // println!("Pressed keyboard key '{:?}'", key);
        };
        if let Some(Button::Keyboard(key)) = e.release_args() {
            self.keys.remove(&key);
            // println!("Released keyboard key '{:?}'", key);
        };
    }
}
