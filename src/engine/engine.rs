// this file will be the main interface to the piston window
extern crate piston_window;
extern crate piston;

use piston_window::*;

use sdl2_window::Sdl2Window;

use engine::input::InputEngine;
use engine::playerposition::*;
use engine::camera::Camera;
use engine::texture::*;
use engine::resolution::*;
// use engine::clock::*;

use entity::character::PlayerCharacter;
use entity::entity::*;

use entity::entities::treasure_chest::*;

use map::map::Map;
// use map::tile::Tile;
// use map::tile::TileType;
use map::prototype::*;

pub struct Engine {
    pub window:          PistonWindow<Sdl2Window>,
    pub camera:          Camera,
    pub input:           InputEngine,
    pub ticks:           u64,
    pub time:            f64,
    pub entities:        Vec<Box<Entity>>,
    pub static_textures: Vec<RenderTexture>,
    // pub tx_clock:        Sender<(String, Sender<u64>)>,
    // pub tx_cd:           Sender<(String, u64, Sender<u64>)>
}

impl Engine {
    // new generates a new default piston window and input engine for our game engine
    pub fn new(x: u32, y: u32) -> Engine {

        let new_engine = Engine {
            window: WindowSettings::new("Spellthief", [x, y]).vsync(true).
                                 exit_on_esc(false).build().unwrap(),
            input: InputEngine::new(),
            camera: Camera::new(0.0, 0.0),
            static_textures: Vec::new(),
            entities: Vec::new(),
            time: 0.0,
            ticks: 0,
            // tx_clock: t_tx,
            // tx_cd: cd_tx,

        };
        let (xres, yres) = get_screen_resolution();
        println!("screen res = {}, {}", xres, yres);
        new_engine
    }

    // run handles the standard in game loop - other functions will handle menus / inventory
    pub fn run(&mut self) {
        // thread::spawn(move || {
        //     engine_clock.run();
        // });

        // engine makes a map to run on
        let map = Map::build_from_file("test_map.map.cfg");
        let proto_tex_map = ProtoTextureMap::build_from_file("test_map.tex.cfg");

        let start = PlayerPosition::new(100.0, 100.0, 0);
        let mut player = PlayerCharacter::new(&mut self.window, start, "player.set.yaml");

        let chest = TreasureChest::new(&mut self.window, 1200.0, 1200.0, "treasure_chest.set.yaml");
        let cb = Box::new(chest);
        self.entities.push(cb);

        for tex in proto_tex_map.map {
            self.static_textures.push(RenderTexture::from_proto_texture(&mut self.window, tex));
        }

        self.camera.jump(player.pos.x, player.pos.y);

        // depending on the type of piston event that is recieved, handle with certain actions
        while let Some(e) = self.window.next() {
            match &e {
                Event::Input(_inp) => {      // if we recieve input, handle it
                    self.input.get(&e);
                },
                Event::Loop(_ren) => {       // while piston is running, update data and render
                    self.update(&e, &map, &mut player);
                    self.render(&e, &map, &mut player);
                },
                _ => {},
            }
        }
    }

    // update will eventually contain a lot more than this, including AI threads, entity update calls, etc
    // for now it just measures input and recalculates speed
    fn update(&mut self, e: &Event, map: &Map, player: &mut PlayerCharacter) {
        if let Some(args) = e.update_args() {
            self.ticks = (args.dt * 1000.0).round() as u64;

            let mut old_entities: Vec<Box<Entity>> = Vec::new();


            old_entities.push(Box::new(player.clone()));

            

            for entity in self.entities.iter_mut() {
                old_entities.push(entity.clone());
            }

            player.update(map, &self.input, &mut self.camera , &old_entities, self.ticks);

            for entity in self.entities.iter_mut() {
                // for entity in old_entities.iter() {
                //     let (x,y) = entity.get_pos();
                //     println!("engine update    x: {}   y: {}", x, y);
                // }
                entity.update(map, &self.input, &mut self.camera , &old_entities, self.ticks);
            }

            self.camera.update(None, &player.pos);
        }  
    }

    // render function will eventually be split into groups like background, behind, char ,front ,foreground
    fn render(&mut self, e: &Event, map: &Map, player: &mut PlayerCharacter) {
        let mut ticks = 1;
        if let Some(args) = e.update_args() {
            ticks = (args.dt * 666.0).round() as u64;
            if ticks > 100 {
                ticks = 20;
            }
        }
        self.window.draw_2d(e, |_c, g| {
            clear([0.0, 0.0, 0.0, 0.0], g);
        });
        let screen_width :f64  = get_screen_width() as f64;
        let screen_height :f64 = get_screen_height() as f64;

        let camx = self.camera.x;
        let camy = self.camera.y;

        let x_min = camx - (screen_width / 2.0);
        let x_max = camx + (screen_width / 2.0);
        let y_min = camy - (screen_height / 2.0);
        let y_max = camy + (screen_height / 2.0);

        let statics = &self.static_textures;


        player.update_frame(ticks);
        let player_box = Box::new(player.clone());
        self.entities.push(player_box);

        // background 
        for texture in statics {
            if texture.is_on_screen(x_min, x_max, y_min, y_max) && texture.layer == RenderLayer::Background {
                self.window.draw_2d(e, |c, g| {
                    image(&texture.texture, c.transform.trans((screen_width as f64 / 2.0 - camx) + texture.pos_x , (screen_height as f64 / 2.0 - camy) + texture.pos_y).scale(texture.scale, texture.scale), g);
                });
            }
        }

        // floor
        for texture in statics {
            if texture.is_on_screen(x_min, x_max, y_min, y_max) && texture.layer == RenderLayer::Floor {
                self.window.draw_2d(e, |c, g| {
                    image(&texture.texture, c.transform.trans((screen_width as f64 / 2.0 - camx) + texture.pos_x , (screen_height as f64 / 2.0 - camy) + texture.pos_y).scale(texture.scale, texture.scale), g);
                });
            }
        }

        // main layer
        for i in 0..map.height * map.tile_size {
            let i = i as f64;
            for entity in self.entities.iter_mut() {
                if entity.get_pos().1 >= i - 1.0 &&  entity.get_pos().1 < i {
                    let frame = entity.get_frame(ticks);
                    self.window.draw_2d(e, |c, g| {
                        image(&frame.texture, c.transform.trans((screen_width as f64 / 2.0 - camx) + frame.pos_x , (screen_height as f64 / 2.0 - camy) + frame.pos_y).scale(frame.scale, frame.scale), g);
                    });
                }
            }
        }

        self.entities.pop();
    }
}
