extern crate find_folder;
extern crate image;

use piston_window::*;
use image::{ImageBuffer};
use image::*;

use sdl2_window::Sdl2Window;

use std::path::PathBuf;

use map::prototype::ProtoTexture;
use engine::resolution::*;


#[derive(Clone)]
pub struct RenderTexture {
    pub size_x:  f64,
    pub size_y:  f64,
    pub pos_x:   f64,
    pub pos_y:   f64,
    pub scale:   f64,
    pub layer:   RenderLayer,
    pub texture: G2dTexture,
}

impl RenderTexture {
    pub fn new( window: &mut PistonWindow<Sdl2Window>, sx: f64, sy: f64, ox: f64, oy: f64, px: f64, py: f64,
               lyr: RenderLayer, png: &str) -> RenderTexture {
        let assets = find_folder::Search::ParentsThenKids(3, 3)
                     .for_folder("assets").unwrap();

        let png_path = assets.join(png);                          // get the path to the png

        let scale_factor = get_screen_width() as f64 / 3840.0;    // scale factor so things are the right size on not 4k monitors

        let px = px * scale_factor;                               // recalculate position
        let py = py * scale_factor;                                   

        // this function will crop down the image for animation frames, or just return the whole image 
        // ox/oy is the offset into the png, sx/sy are the size of the subimage to grab
        let scaled_png = RenderTexture::get_sub_image(png_path, ox as u32, oy as u32, sx as u32, sy as u32);

        let new_tex: G2dTexture = Texture::from_image(
                 &mut window.factory,
                 &scaled_png,
                 &TextureSettings::new()
            ).unwrap();

        RenderTexture {
            size_x: sx,
            size_y: sy,
            pos_x: px,
            pos_y: py,
            scale: scale_factor,
            layer: lyr,
            texture: new_tex,
        }
    }

    // returns a cropped image buffer that can be turned into a texture
    fn get_sub_image(png: PathBuf, ox: u32, oy: u32, width: u32, height: u32)  -> ImageBuffer<Rgba<u8>, Vec<u8>> {
        let mut tex_png = image::open(png).unwrap();
        
        let sub_img = imageops::crop(&mut tex_png, ox, oy, width, height);

        sub_img.to_image()
    }

    // moves a textures render location
    pub fn change_pos(&mut self, x: f64, y: f64) {
        self.pos_x = x;
        self.pos_y = y;
    }

    // builds a new texture from a proto texture
    pub fn from_proto_texture(window: &mut PistonWindow<Sdl2Window>, proto: ProtoTexture) -> RenderTexture {
        RenderTexture::new(window, proto.size_x, proto.size_y, 0.0, 0.0, proto.pos_x, proto.pos_y, proto.layer, &proto.png)
    }

    // determines if an entity is on screen to decide whether or not it needs to be drawn
    // TODO: make this actually do something...  
    pub fn is_on_screen(&self, _xmin: f64, _xmax: f64, _ymin: f64, _ymax: f64) -> bool {
        return true;
    }
}

#[allow(dead_code)]
#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub enum RenderLayer {
    Background,
    Floor,
    Main,
    Above,
    Top,
}

impl RenderLayer {
    pub fn get_layer(input: u8) -> RenderLayer {
        match input {
            0 => RenderLayer::Background,
            1 => RenderLayer::Floor,
            2 => RenderLayer::Main,
            3 => RenderLayer::Above,
            4 => RenderLayer::Top,
            _ => RenderLayer::Main,
        }
    }
}


