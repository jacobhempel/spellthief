extern crate find_folder;

use sdl2_window::Sdl2Window;
use piston_window::*;

use engine::playerposition::*;
use engine::texture::*;

#[derive(Clone)]
pub struct Animation {
    pub anitype:  AnimationType,
    pub facing:   Direction,
    pub frames:   Vec<Frame>,
}

impl Animation {
    // pushes a frame into the frames vec - called by AnimationSet::build_from_file
    pub fn add_frame(&mut self, window: &mut PistonWindow<Sdl2Window>, sx: f64, sy: f64, ox: f64, oy: f64,
                 lyr: RenderLayer, dur: u64, png: &str) {
        self.frames.push(Frame::new(window, sx, sy, ox, oy, lyr, png, dur))
    }
}


// single frame of an animation - just a texture and a duration (in ms)
#[derive(Clone)]
pub struct Frame {
    pub texture: RenderTexture,
    pub duration: usize,
}

impl Frame {
    pub fn new(window: &mut PistonWindow<Sdl2Window>, sx: f64, sy: f64, ox: f64, oy: f64,
               lyr: RenderLayer, png: &str, dur: u64) -> Frame {
        Frame {
            texture: RenderTexture::new(window, sx, sy, ox, oy, 0.0, 0.0, lyr, png),
            duration: dur as usize,
        }
    }
}

// amimation types to determine what animations to play
#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub enum AnimationType {
    Idle,
    Walk,
    Run,
    Attack,
    Cast,
    Interact,
}

impl AnimationType {
    pub fn as_int(self) -> i32 {
        match self {
            AnimationType::Idle     => 0,
            AnimationType::Walk     => 1,
            AnimationType::Run      => 2,
            AnimationType::Attack   => 3,
            AnimationType::Cast     => 4,
            AnimationType::Interact => 5,
        }
    }
}

pub fn anitype_from_str(input: &str) -> AnimationType {
    match input {
        "Idle"     => AnimationType::Idle,
        "Walk"     => AnimationType::Walk,
        "Attack"   => AnimationType::Attack,
        "Run"      => AnimationType::Run,
        "Cast"     => AnimationType::Cast,
        "Interact" => AnimationType::Interact,
        _          => AnimationType::Idle,
    }
}

