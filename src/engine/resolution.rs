// this files contains a global variable of the screen resolution
// lots of unsafe in here because mutable globals are SCARY
static mut SCREEN_RESOLUTION: (u32, u32) = (0,0);

pub fn get_screen_resolution() -> (u32, u32) {
    let mut ret = (0, 0);
    unsafe {
        ret.0 = SCREEN_RESOLUTION.0;
        ret.1 = SCREEN_RESOLUTION.1;
    }
    ret
}

pub fn get_screen_width() -> u32 {
    let ret;
    unsafe {
        ret = SCREEN_RESOLUTION.0;
    }
    ret
}

pub fn get_screen_height() -> u32 {
    let ret;
    unsafe {
        ret = SCREEN_RESOLUTION.1;
    }
    ret
}


// THIS FUNCTION IS VERY DANGEROUS - PLEASE USE WITH CAUTION
pub fn set_screen_resolution(width: u32, height: u32) {
    unsafe {
        SCREEN_RESOLUTION.0 = width;
        SCREEN_RESOLUTION.1 = height;
    }
}
