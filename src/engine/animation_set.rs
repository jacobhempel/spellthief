extern crate find_folder;
extern crate yaml_rust;

use std::str::FromStr;
use std::fs::*;
use std::error::Error;
use std::io::Read;
use std::collections::HashMap;

use sdl2_window::Sdl2Window;
use piston_window::*;
use nom::*;
use nom::types::*;
use yaml_rust::{YamlLoader};

use engine::playerposition::*;
use engine::texture::*;
use engine::animation::*;

#[derive(Clone)]
pub struct AnimationSet {
    last_dir:   Direction,     // used for determining the current frame
    last_state: AnimationType, // ^^

    frame_counter: usize,      // current frame in set
    frame_timer: usize,        // timing how long the current frame has been running

    pub offset_x: f32,         // animation offsets - animation starts drawing to the top left of the "position"
    pub offset_y: f32,         // y direction for above

    set:        HashMap<(i32, i32), Animation>, // animations are stored in this map 
                                                // two keys are direction and type converted to i32
}

impl AnimationSet {
    pub fn new(file_name: &str, window: &mut PistonWindow<Sdl2Window>) -> AnimationSet {
        let set = AnimationSet::build_set_from_file(file_name, window);
        let ani_set = AnimationSet {
            set: set.0,   // opens the cfg file for the animation and plugs the contents into the set

            frame_counter: 0,
            frame_timer: 0,

            offset_x: set.1,
            offset_y: set.2,

            last_dir: Direction::South,
            last_state: AnimationType::Idle,
        };
        ani_set
    }


    pub fn build_set_from_file(file_name: &str, window: &mut PistonWindow<Sdl2Window>) -> (HashMap<(i32, i32), Animation>, f32, f32) {
        let configs = find_folder::Search::ParentsThenKids(3,3).for_folder("configs").unwrap();
        let config_path = configs.join(file_name);

        let mut file = match File::open(&config_path) {
            Err(why) => panic!("couldn't open file because {}", why.description()),
            Ok(file) => file,
        };
        let mut buf = String::new();
        file.read_to_string(&mut buf).unwrap();
        let docs = YamlLoader::load_from_str(&buf).unwrap();
        let doc = &docs[0];

        let sprite_sheet = &doc["sprite_sheet"].as_str().unwrap();
        let anim_offset_x = *&doc["animation_offset_x"].as_f64().unwrap() as f32;
        let anim_offset_y = *&doc["animation_offset_y"].as_f64().unwrap() as f32;
        let canvas_x = *&doc["canvas_x"].as_i64().unwrap();
        let canvas_y = *&doc["canvas_y"].as_i64().unwrap();

        let mut anim_set = HashMap::<(i32, i32), Animation>::new();

        let mut i = 0;
        while !&doc["animations"][i].is_badvalue() {
            let mut j = 0;
            let ani_type = &doc["animations"][i]["type"].as_str().unwrap();
            let ani_type = anitype_from_str(ani_type);
            let facing = &doc["animations"][i]["face"].as_str().unwrap();
            let facing = dir_from_str(facing);
            
            let mut animation = Animation {    // new empty animation to hold the data we read from the file
                    anitype: ani_type,   
                    facing: facing,
                    frames: Vec::new(),
            };
            while !&doc["animations"][i]["frames"][j].is_badvalue() {
                let offset_x = &doc["animations"][i]["frames"][j]["offset_x"].as_i64().unwrap();
                let offset_y = &doc["animations"][i]["frames"][j]["offset_y"].as_i64().unwrap();
                let layer_f = *&doc["animations"][i]["frames"][j]["offset_x"].as_i64().unwrap();
                let dur = *&doc["animations"][i]["frames"][j]["duration"].as_i64().unwrap();
                let layer = RenderLayer::get_layer(layer_f as u8);
                animation.add_frame(window, canvas_x as f64, canvas_y as f64, *offset_x as f64, *offset_y as f64, layer, dur as u64, sprite_sheet);
                j += 1;
            }
            anim_set.insert((facing.as_int(), ani_type.as_int()), animation);
            i += 1;
        }
        (anim_set, anim_offset_x, anim_offset_y)
    }

    pub fn update_frame(&mut self, new_dir: Direction, new_state: AnimationType, ticks: u64) -> RenderTexture {
        if new_state != self.last_state {  // if the state has changed, reset the counters        
            self.last_state = new_state;   // and timers and return the first frame of the correct animation 
            self.last_dir = new_dir;
            self.frame_counter = 0;
            self.frame_timer = 0;
            let d = self.last_dir.as_int();
            let s = self.last_state.as_int();
            let c = self.frame_counter;
            self.set[&(d,s)].frames[c].texture.clone()
        } else {  // if the state hasnt changed, calculated which frame should be live, and return it
            self.last_dir = new_dir;

            self.frame_timer += ticks as usize;
            let d = self.last_dir.as_int();
            let s = self.last_state.as_int();
            while self.frame_timer > self.set[&(d,s)].frames[self.frame_counter].duration {
                self.frame_timer -= self.set[&(d,s)].frames[self.frame_counter].duration;
                self.frame_counter = (self.frame_counter + 1) % self.set[&(d,s)].frames.len();
            }
            let c = self.frame_counter;
            self.set[&(d,s)].frames[c].texture.clone()

        }
    }

    pub fn get_active_frame(&self) -> RenderTexture {
        let d = self.last_dir.as_int();
        let s = self.last_state.as_int();
        let c = self.frame_counter;
        self.set[&(d,s)].frames[c].texture.clone()
    }
}

named!(parse_animation(CompleteStr) -> (f64, f64, u8, u64),
    tuple!(
        ws!(double),
        ws!(double),
        ws!(map_res!(digit, |CompleteStr(s)| u8::from_str(s))),
        ws!(map_res!(digit, |CompleteStr(s)| u64::from_str(s)))
    )
);