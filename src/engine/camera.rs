use engine::playerposition::PlayerPosition;
use engine::playerposition::Direction;

use engine::resolution::*;

pub struct Camera {
    pub x: f64,
    pub y: f64,
    x_offset: f64,
    y_offset: f64,
    max_offset: f64,
    max_offset_diag: f64,
    cam_vel: f64,
}

impl Camera {
    pub fn new(x: f64, y:f64) -> Camera {
        Camera {
            x: x,
            y: y,
            x_offset: 0.0,
            y_offset: 0.0,
            max_offset: (get_screen_width() / 30) as f64,
            max_offset_diag: ((get_screen_width() / 30) as f64) / 1.41,
            cam_vel: get_screen_width() as f64 / 1600.0,
        }
    }
    pub fn update(&mut self, caputure_zone: Option<CameraCaptureZone>, pos: &PlayerPosition) {
        let goal_x :f64;
        let goal_y :f64;
        match caputure_zone {     // if theres a special camera capture zone, go to that instead
            Some(poi) => {
                goal_x = poi.camera_x;
                goal_y = poi.camera_y;
            }
            None => {             // otherwise, calculate the the goal positions based on camera offset 
                goal_x = pos.x + self.x_offset;
                goal_y = pos.y + self.y_offset;
            }
        }  
        // determine how far camera needs to move, and divide by a number to keep things SMOOTH
        let mut delta_x = goal_x - self.x;
        let mut delta_y = goal_y - self.y;
        delta_x /= 40.0; // THIS VALUE CHANGES CAMERA ACCELERATION
        delta_y /= 40.0; // BIGGER MEANS SLOWER
        // println!("delta x = {:.4}   ---    delta y = {:.4}", delta_x, delta_y);
        self.mv(delta_x, delta_y);
    }

    // some bullshit right here, but essentially this sets the target offset of the camera based on the players current facing
    pub fn facing(&mut self, dir: Direction) {
        match dir {
            Direction::North => {
                self.y_offset -= self.cam_vel;
                if self.y_offset < -self.max_offset {
                    self.y_offset = -self.max_offset;
                }
            }
            Direction::Northeast => {
                if self.y_offset < -self.max_offset_diag {
                    self.y_offset += self.cam_vel;
                } else {
                    self.y_offset -= self.cam_vel;
                    if self.y_offset < -self.max_offset_diag {
                        self.y_offset = -self.max_offset_diag;
                    }
                }

                if self.x_offset > self.max_offset_diag {
                    self.x_offset -= self.cam_vel;
                } else {
                    self.x_offset += self.cam_vel;
                    if self.x_offset > self.max_offset_diag {
                        self.x_offset = self.max_offset_diag;
                    }
                }
            }
            Direction::East => {
                self.x_offset += self.cam_vel;
                if self.x_offset > self.max_offset {
                    self.x_offset = self.max_offset;
                }
            }
            Direction::Southeast => {
                if self.y_offset > self.max_offset_diag {
                    self.y_offset -= self.cam_vel;
                } else {
                    self.y_offset += self.cam_vel;
                    if self.y_offset > self.max_offset_diag {
                        self.y_offset = self.max_offset_diag;
                    }
                }

                if self.x_offset > self.max_offset_diag {
                    self.x_offset -= self.cam_vel;
                } else {
                    self.x_offset += self.cam_vel;
                    if self.x_offset > self.max_offset_diag {
                        self.x_offset = self.max_offset_diag;
                    }
                }
            }
            Direction::South => {
                self.y_offset += self.cam_vel;
                if self.y_offset > self.max_offset {
                    self.y_offset = self.max_offset;
                }
            }
            Direction::Southwest => {
                if self.y_offset > self.max_offset_diag {
                    self.y_offset -= self.cam_vel;
                } else {
                    self.y_offset += self.cam_vel;
                    if self.y_offset > self.max_offset_diag {
                        self.y_offset = self.max_offset_diag;
                    }
                }
                if self.x_offset < -self.max_offset_diag {
                    self.x_offset += self.cam_vel;
                } else {
                    self.x_offset -= self.cam_vel;
                    if self.x_offset < -self.max_offset_diag {
                        self.x_offset = -self.max_offset_diag;
                    }
                }
            }
            Direction::West => {
                self.x_offset -= self.cam_vel;
                if self.x_offset < -self.max_offset {
                    self.x_offset = -self.max_offset;
                }
            }
            Direction::Northwest => {
                if self.y_offset > self.max_offset_diag {
                    self.y_offset -= self.cam_vel;
                } else {
                    self.y_offset += self.cam_vel;
                    if self.y_offset > self.max_offset_diag {
                        self.y_offset = self.max_offset_diag;
                    }
                }

                if self.x_offset < -self.max_offset_diag {
                    self.x_offset += self.cam_vel;
                } else {
                    self.x_offset -= self.cam_vel;
                    if self.x_offset < -self.max_offset_diag {
                        self.x_offset = -self.max_offset_diag;
                    }
                }
            }
        }
    }

    // moves the camera (WILD I KKOW)
    pub fn mv(&mut self, x: f64, y: f64) {  
        self.x += x;
        self.y += y;   
    }
    // teleports the camera to a set location
    pub fn jump(&mut self, x: f64, y: f64) {
        self.x = x;
        self.y = y;
    }
}

pub struct CameraCaptureZone {
    pub camera_x: f64,
    pub camera_y: f64,
    pub zone_max_x: f64,
    pub zone_max_y: f64,
    pub zone_min_x: f64,
    pub zone_min_y: f64,
}
