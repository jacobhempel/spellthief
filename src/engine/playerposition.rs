use std::f64;

use map::map::Map;
use engine::resolution::*;

#[derive(Clone)]
pub struct PlayerPosition {
    pub x    : f64,
    pub x_vel: f64,

    pub y    : f64,
    pub y_vel: f64,

    pub accel     :f64,
    pub top_spd   :f64,

    pub facing : Facing,

    pub elev: i16,
}

impl PlayerPosition {
    pub fn new(x_new: f64, y_new: f64, e_new: i16) -> PlayerPosition {
        PlayerPosition {
            x: x_new,
            y: y_new,
            x_vel: 0.0,
            y_vel: 0.0,
            accel: get_screen_width() as f64 / 512000.0,
            top_spd: get_screen_width() as f64 / 3600.0,
            facing : Facing::new(),
            elev: e_new,
        }
    }

    // this function updates the position of the character - checks to make sure its passable first
    // works in x and y separately to make sure we can slide along walls and stuff
    pub fn update(&mut self, map: &Map, ticks: u64) {
        let x_vel = self.x_vel;
        let y_vel = self.y_vel;
        if map.is_passable(self.x + x_vel * (ticks as f64), self.y) {
            // print!("moving {:.4} in x   ", x_vel * (ticks as f64));
            self.mv(x_vel * (ticks as f64), 0.0);
        }
        else {
            self.x_vel = 0.0;
        }
        if map.is_passable(self.x , self.y + y_vel * (ticks as f64)) {
            // println!("moving {:.4} in y   ", y_vel * (ticks as f64));
            self.mv(0.0, y_vel * (ticks as f64));
        }
        else {
            self.y_vel = 0.0;
        }
    }

    // this function changes the velocity of the character and recalculates for diagonal
    pub fn accelerate(&mut self, dx: f64, dy: f64, ticks: u64) {
        self.x_vel += dx * (ticks as f64);
        if self.x_vel > self.top_spd {
            self.x_vel = self.top_spd
        }
        if self.x_vel < -self.top_spd {
            self.x_vel = -self.top_spd
        }
        self.y_vel += dy * (ticks as f64);
        if self.y_vel > self.top_spd {
            self.y_vel = self.top_spd
        }
        if self.y_vel < -self.top_spd {
            self.y_vel = -self.top_spd
        }
        if (self.y_vel.abs().powi(2) + self.x_vel.abs().powi(2)).sqrt() >= self.top_spd {
            self.x_vel /= 1.4;
            self.y_vel /= 1.4;
        }
    }

    pub fn mv(&mut self, dx: f64, dy: f64) {
        self.x += dx;
        self.y += dy;
    }
}

// a struct to smooth out the direction system
#[derive(Clone)]
pub struct Facing {
    dir: Direction,
    next: Direction,
    count: u8,
}

impl Facing {
    pub fn new() -> Facing {
        Facing {
            dir: Direction::South,
            next: Direction::South,
            count: 0,
        }
    }
    pub fn get(&self) -> Direction {
        return self.dir;
    }
    pub fn set(&mut self, input: Direction) {
        if input == self.next {
            self.count += 1;
        } else {
            self.next = input;
            self.count = 0;
        }
        if self.count > 250 {
            self.count = 20;
        }
        if self.count > 10 {
            self.dir = self.next;
        }
    }
}


#[derive(Debug, Eq, PartialEq, PartialOrd, Clone, Copy)]
pub enum Direction {
    North,
    Northeast,
    East,
    Southeast,
    South,
    Southwest,
    West,
    Northwest,
}

impl Direction {
    pub fn as_int(self) -> i32 {
        match self {
            Direction::North     => 0,
            Direction::Northeast => 1,
            Direction::East      => 2,
            Direction::Southeast => 3,
            Direction::South     => 4,
            Direction::Southwest => 5,
            Direction::West      => 6,
            Direction::Northwest => 7,
        }
    }

    // PRIMARILY FOR DEBUGGING 
    #[allow(dead_code)]
    pub fn as_str(self) -> String {
        match self {
            Direction::North     => "North".to_string(),
            Direction::Northeast => "Northeast".to_string(),
            Direction::East      => "East".to_string(),
            Direction::Southeast => "Southeast".to_string(),
            Direction::South     => "South".to_string(),
            Direction::Southwest => "Southwest".to_string(),
            Direction::West      => "West".to_string(),
            Direction::Northwest => "Northwest".to_string(),
        }
    }

    pub fn cardinal(self) -> Direction {
         match self {
            Direction::North     => Direction::North,
            Direction::Northeast => Direction::East,
            Direction::East      => Direction::East,
            Direction::Southeast => Direction::East,
            Direction::South     => Direction::South,
            Direction::Southwest => Direction::West,
            Direction::West      => Direction::West,
            Direction::Northwest => Direction::West,
        }
    }
}

pub fn  dir_from_str(input: &str) -> Direction {
    match input {
        "North" => Direction::North,
        "Northeast" => Direction::Northeast,
        "East" => Direction::East,
        "Southeast" => Direction::Southeast,
        "South" => Direction::South,
        "Southwest" => Direction::Southwest,
        "West" => Direction::West,
        "Northwest" => Direction::Northwest,
        _           => Direction::South,
    }
}
