// externs
extern crate piston_window;
extern crate piston;
extern crate sdl2_window;
extern crate opengl_graphics;
extern crate find_folder;
extern crate image;
extern crate crossbeam;
extern crate yaml_rust;

extern crate nom;

// modules
mod engine;
mod map;
mod entity;

// extern uses
use std::env;

// local module uses
use engine::engine::Engine;
use engine::resolution::*;
// use engine::clock::*;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 3 {
        set_screen_resolution(args[1].parse::<u32>().unwrap(), args[2].parse::<u32>().unwrap());
    }
    else {
        set_screen_resolution(1920, 1080);
    }

    let mut engine = Engine::new(get_screen_width(), get_screen_height());
    engine.run();
}
